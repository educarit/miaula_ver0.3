<!-- main/layout/topbar.tpl -->
<!-- Topbar -->
<nav id="toolbar-admin" class="navbar-sie">
  <a class="navbar-brand" href="#">
    <!-- <img src="" class="logo-sie d-inline-block align-top" width="320" alt="" /> -->
    <div class="logo-sie d-inline-block align-top"></div>
  </a>
  <ul class="navbar-nav mr-auto">
  </ul>

  {% if _u.logged %}
  <div class="btns-header">
    
    <div class="dropdown-horizontal dropdown dropleft">
      <!-- <div class="tools-menu">
        <div class="btn-tools" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="dropdownMenu">
          <ul>
            <li><a href="#" target="_blank"><img src="" />Galería</a></li>
            <li><a href="#" target="_blank"><img src="" />Pruebas Saber</a></li>
            <li><a href="#" target="_blank"><img src="" />Mi aula</a></li>
            <li><a href="#" target="_blank"><img src="" />Mi biblioteca</a></li>
            <li><a href="#" target="_blank"><img src="" />Gran consultor</a></li>
          </ul>
        </div>
      </div> -->
      <!-- <a class="provisional-btn-topbar" href="{{  _p.web }}index.php?logout=logout&uid={{_u.user_id}}">{{ "Logout"|get_lang }}</a> -->
      <div class="user-preferences-menu">
        <!-- <a href="{{  _p.web }}index.php?logout=logout&uid={{_u.user_id}}">Cerrar</a> -->
        <!-- <div class="avatar" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="dropdownMenu2">
          <ul>
            <li><a id="BtnAvatar" href="#"><img src="" />Personalizar</a></li>
            <li><a id="Administrador" href="#" target="_blank"><img src="" />Administrador</a></li>
            <li><a id="admin_saber" href="#" target="_blank"><img src="" />Admin Saber</a></li>
            <li><a id="informes" href="#" target="_blank"><img src="" />Informes</a></li>
            <li><a id="panamericana" href="#" target="_blank"><img src="" />Panamericana</a></li>
            <li><a id="Autores" href="#" target="_blank"><img src="" />Autores</a></li>
            <li><a id="parametrizacion" href="#" target="_blank"><img src="" />Gestión Institucional</a></li>
            <li><a id="Planeador" href="#" target="_blank"><img src="" />Planeador</a></li>
            <li><a id="Informe Académico" href="#" target="_blank"><img src="" />Informe Académico</a></li>
            <li><a id="ayuda" href="#" target="_self"><img src="" />Tutoriales</a></li>
            <li><a id="BtnSoporte" href="#" target="_self"><img src="" />Soporte</a></li>
            <li><a href="{{  _p.web }}index.php?logout=logout&uid={{_u.user_id}}">{{ "Logout"|get_lang }}</a></li>
          </ul>
        </div> -->

      </div>
    </div>
    {% endif %}
  </div>
</nav>